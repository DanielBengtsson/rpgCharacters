# rpgCharacters c\# Application

## Contributors

Daniel Bengtsson - daniel.johan.gunnar.bengtsson@se.experis.com

## Usage
Console application where you are able to create four different characters `Mage, Warrior, Ranger, Rogue`.
Characters are able to equip Weapons and different armor pieces.
You are also able to `LevelUp()` your character to equip higher level armor pieces.

### Example

`Warrior warrior = new("Name")`

`warrior.LevelUp()`

`warrior.equipItem(axe)` Items need to be defined using the Weapon class.

`Weapon axe = new()
            {
                ItemName = "Common Sword",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON.ToString(),
                WeaponType = WeaponTypes.SWORD.ToString(),
                WeaponAttributes = new WeaponAttributes()
                {
                    AttackSpeed = 1.1,
                    Damage = 7,
                }
            };`

`warrior.PrintInfo()`
