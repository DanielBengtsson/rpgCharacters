﻿using rpgCharacters.Heroes;
using rpgCharacters.Equipment;
using rpgCharacters.Enums;
using rpgCharacters.CustomException;
using rpgCharacters;
using System;


namespace rpgCharactersTests
{
    public class RpgCharacterTests
    {
        [Fact]
        public void CreateCharacter_checkIfLevelOneOnCreation_ShouldReturnTrueIfHeroIslevelOne() { 
            //Arrange
            Warrior warrior = new("Name");
            int expected = 1;

            //Act
            int actual = warrior.Level;

            //Asset
            Assert.Equal(expected, actual);
        }

       [Fact]
       public void LevelUpCharacter_checkIfLevelIncrease_ShouldReturnHeroLevelTwo()
        {
            Warrior warrior = new("name");
            int expected = 2;
            warrior.LevelUp();

            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorCreation_CheckIfCorrectAttributes_ShouldReturnCorrectString()
        {
            Warrior warrior = new ("name");
            string expected = $"strength = 5, dexterity = 2, intellect = 1";

            string actual = warrior.GetStats(warrior.Strength, warrior.Dexterity, warrior.Intelligence);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void MageCreation_CheckIfCorrectAttributes_ShouldReturnCorrectString()
        {
            Mage mage = new("name");
            string expected = $"strength = 1, dexterity = 1, intellect = 8";

            string actual = mage.GetStats(mage.Strength, mage.Dexterity, mage.Intelligence);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RangerCreation_CheckIfCorrectAttributes_ShouldReturnCorrectString()
        {
            Ranger ranger = new("name");
            string expected = $"strength = 1, dexterity = 7, intellect = 1";

            string actual = ranger.GetStats(ranger.Strength, ranger.Dexterity, ranger.Intelligence);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RogueCreation_CheckIfCorrectAttributes_ShouldReturnCorrectString()
        {
            Rogue rogue = new("name");
            string expected = $"strength = 2, dexterity = 6, intellect = 1";

            string actual = rogue.GetStats(rogue.Strength, rogue.Dexterity, rogue.Intelligence);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorLevelUp_CheckIfCorrectStatsAfterLevelUp_ShouldReturnCorrectAttributes() 
        {
            Warrior warrior = new("name");
            string expected = $"strength = 8, dexterity = 4, intellect = 2";
            warrior.LevelUp();

            string actual = warrior.GetStats(warrior.Strength, warrior.Dexterity, warrior.Intelligence);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RangerLevelUp_CheckIfCorrectStatsAfterLevelUp_ShouldReturnCorrectAttributes()
        {
            Ranger ranger = new("name");
            string expected = $"strength = 2, dexterity = 12, intellect = 2";
            ranger.LevelUp();

            string actual = ranger.GetStats(ranger.Strength, ranger.Dexterity, ranger.Intelligence);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void MageLevelUp_CheckIfCorrectStatsAfterLevelUp_ShouldReturnCorrectAttributes()
        {
            Mage mage = new("name");
            string expected = $"strength = 2, dexterity = 2, intellect = 13";
            mage.LevelUp();

            string actual = mage.GetStats(mage.Strength, mage.Dexterity, mage.Intelligence);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RogueLevelUp_CheckIfCorrectStatsAfterLevelUp_ShouldReturnCorrectAttributes()
        {
            Rogue rogue = new("name");
            string expected = $"strength = 3, dexterity = 10, intellect = 2";
            rogue.LevelUp();

            string actual = rogue.GetStats(rogue.Strength, rogue.Dexterity, rogue.Intelligence);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void WarriorEquipWeapon_CheckIfWarriorCanEquipToHighLevelAxe_ShouldReturnAException() 
        {
            Warrior warrior_two = new("name");
            Weapon testAxe = new ()
            {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = Slot.WEAPON.ToString(),
                WeaponType = WeaponTypes.AXE.ToString(),
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            Assert.Throws<InvalidWeaponException>(() => warrior_two.EquipItem(testAxe));
        }
        [Fact]
        public void CharacterEquipGear_CheckIfWarriorCanEquipToHighLevelArmor_ShouldReturnAException()
        {
            Warrior warrior = new("Name");
            Armor testPlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = Slot.CHEST.ToString(),
                ArmorType = ArmorType.PLATE.ToString(),
                Attributes = new Attributes() { Strength = 1 }
            };

            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateBody));

        }

        [Fact]
        public void CharacterEquipGear_CheckIfWarriorCanEquipBow_ShouldReturnAException()
        {
            Warrior warrior = new("Name");
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON.ToString(),
                WeaponType = WeaponTypes.BOW.ToString(),
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };

            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testBow));
        }
        [Fact]
        public void CharacterEquipGear_CheckIfWarriorCanEquipClothArmor_ShouldReturnAException()
        {
            Warrior warrior = new("Name");
            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = Slot.HEAD.ToString(),
                ArmorType = ArmorType.CLOTH.ToString(),
                Attributes = new Attributes() { Intelligence = 5 }
            };

            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testClothHead));
        }
        [Fact]
        public void CharacterEquipWeapon_IfValidWeaponIsEquipped_ShouldReturnASuccessMessage()
        {
            Rogue rogue = new("name");
            Weapon dagger = new()
            {
                ItemName = "Common Axe",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON.ToString(),
                WeaponType = WeaponTypes.DAGGER.ToString(),
                WeaponAttributes = new WeaponAttributes()
                {
                    AttackSpeed = 1.1,
                    Damage = 7,
                }
            };
            string expected = "New weapon equipped!";

            string actual = rogue.EquipItem(dagger);

            Assert.Equal(expected, actual);

        }
        [Fact]
        public void CharacterEquipGear_IfValidGearIsEquipped_ShouldReturnASuccessMessage()
        {
            Ranger ranger = new("name");
            Armor leatherChest = new()
            {
                ItemName = "Chest of Dexterity",
                ItemLevel = 1,
                ItemSlot = Slot.CHEST.ToString(),
                ArmorType = ArmorType.LEATHER.ToString(),
                Attributes = new Attributes() { Intelligence = 5, Strength = 2, Dexterity = 43 },

            };

            string expected = "New armor equipped!";

            string actual = ranger.EquipItem(leatherChest);

            Assert.Equal(expected, actual);

        }
        [Fact]
        public void CalculateDamage_CalculateDamageWithoutWeaponEquipped_ShouldReturnCorrectStats()
        {
            Warrior warrior = new("name");
            double expected = 1.05;
          
            double actual = warrior.GetDamage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamage_CalculateDamageWithWeaponEquipped_ShouldReturnCorrectStats()
        {
            Warrior warrior = new("name");
            double expected = 8.085;
            Weapon axe = new()
            {
                ItemName = "Common Axe",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON.ToString(),
                WeaponType = WeaponTypes.AXE.ToString(),
                WeaponAttributes = new WeaponAttributes()
                {
                    AttackSpeed = 1.1,
                    Damage = 7,
                }
            };
            warrior.EquipItem(axe);

            double actual = warrior.GetDamage();

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamage_DamageWithWeaponAndArmor_ShouldReturnCorrectStats()
        {
            Warrior warrior = new("name");
            double expected = 8.162;


            Weapon axe = new()
            {
                ItemName = "Common Axe",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON.ToString(),
                WeaponType = WeaponTypes.AXE.ToString(),
                WeaponAttributes = new WeaponAttributes()
                {
                    AttackSpeed = 1.1,
                    Damage = 7,
                }
            };
            Armor testPlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.CHEST.ToString(),
                ArmorType = ArmorType.PLATE.ToString(),
                Attributes = new Attributes() { Strength = 1 }
            };
            warrior.EquipItem(axe);
            warrior.EquipItem(testPlateBody);

            double actual = warrior.GetDamage();

            Assert.Equal(expected, actual);

        }


    }
}
