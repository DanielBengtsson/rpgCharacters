﻿using rpgCharacters.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpgCharacters.Heroes
{
    public class Attributes : WeaponAttributes
    {
        //Getters & Setters
        public int Strength { get; set; }
        public int Intelligence { get; set; }
        public int Dexterity { get; set; }
    }

}
