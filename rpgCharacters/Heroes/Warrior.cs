﻿using rpgCharacters.Equipment;
using rpgCharacters.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rpgCharacters.CustomException;


namespace rpgCharacters.Heroes
{
    public class Warrior : Character
    {
        public Warrior() { }
        //Overloaded constructor. Creates a Warrior.
        public Warrior(string name)
        {
            Class = "Warrior";
            Name = name;
            Level = 1;
            Strength = 5;
            Dexterity = 2;
            Intelligence = 1;
        }
        /// <summary>
        ///     Function that levels upp character
        /// </summary>
        public void LevelUp()
        {
            Level++;
            Strength += 3;
            Dexterity += 2;
            Intelligence++;
        }
        /// <summary>
        ///     Function to get the stats, needs the stats from the hero you want to check.
        /// </summary>
        /// <param name="str">Strength</param>
        /// <param name="dex">Intelligence</param>
        /// <param name="intellect"></param>
        /// <returns></returns>
        public string GetStats(int str,int dex,int intellect)
        {
            return $"strength = {str}, dexterity = {dex}, intellect = {intellect}";
        }
        /// <summary>
        ///     Function that equips items. Needs an Item.
        /// </summary>
        /// <param name="item">The Item that gets passed as a parameter</param>
        /// <returns></returns>
        /// <exception cref="InvalidWeaponException">Throws and error if Weapon item cannot be equipped</exception>
        /// <exception cref="InvalidArmorException">Throws and error if Gear item cannot be equipped</exception>
        public string EquipItem(Item item)
        {
            Dictionary<string, Item> equipment = new Dictionary<string, Item>();
            Dictionary<string, Item> weapons = new Dictionary<string, Item>();

            if (item.ItemSlot == Slot.WEAPON.ToString())
            {
               if(item.WeaponType == WeaponTypes.AXE.ToString() || item.WeaponType == WeaponTypes.HAMMER.ToString() || item.WeaponType == WeaponTypes.SWORD.ToString())
                {
                    if (Level < item.ItemLevel)
                    {
                        throw new InvalidWeaponException("level to low");
                    }
                    else
                    {
                        weapons.Add(item.ItemSlot, item);
                        UpDateStatsWeapon(weapons);
                        return $"New weapon equipped!";
                    }
                }
                else
                {
                    throw new InvalidWeaponException("Can't equip that type of weapon");
                }
            }
            else
            {
                if (item.ArmorType == ArmorType.PLATE.ToString() || item.ArmorType == ArmorType.MAIL.ToString())
                {
                    if (Level < item.ItemLevel)
                    {
                        throw new InvalidArmorException("Item level is to high");
                    }
                    else
                    {
                        equipment.Add(item.ItemSlot, item);
                        UpDateStats(equipment);
                        return $"New armor equipped!";
                    }
                }
                else
                {
                    throw new InvalidArmorException("Can't equip that type of Armor");
                }
            }
        }
        /// <summary>
        ///     Loop through dictionary of gear and add stats to Character. is used within equipItem. 
        /// </summary>
        /// <param name="items">Dictionary<string, Item></param>
        public void UpDateStats(Dictionary<string, Item> items)
        {
            foreach (var item in items)
            {
                Intelligence += item.Value.Attributes.Intelligence;
                Dexterity += item.Value.Attributes.Dexterity;
                Strength += item.Value.Attributes.Strength;
            }
        }
        /// <summary>
        ///     Loop through dictionary of Weapon and add stats to Character. is used within equipItem. 
        /// </summary>
        /// <param name="items">Dictionary<string, Item></param>
        public void UpDateStatsWeapon(Dictionary<string, Item> items)
        {
            foreach (var item in items)
            {
                Damage += item.Value.WeaponAttributes.Damage;
                AttackSpeed += item.Value.WeaponAttributes.AttackSpeed;
            }
        }
    }


}
