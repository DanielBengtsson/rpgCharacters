﻿using rpgCharacters.Enums;
using rpgCharacters.Equipment;
using rpgCharacters.Heroes;

namespace rpgCharacters
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Create ranger
            Ranger ranger = new("Daniel Bengtsson");
            
            //Create a Sword of type BOW.
            Weapon sword = new() 
            {
                ItemName = "Common Sword",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON.ToString(),
                WeaponType = WeaponTypes.BOW.ToString(),
                WeaponAttributes = new WeaponAttributes()
                {
                    AttackSpeed = 1.1,
                    Damage = 7,
                }
            };
            //Create leather chest of type leather.
            Armor LeatherChest = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.CHEST.ToString(),
                ArmorType = ArmorType.LEATHER.ToString(),
                Attributes = new Attributes() { Strength = 5,Dexterity = 10}
            };
            //Create "high level" legs of type mail armor.
            Armor legsMail = new Armor()
            {
                ItemName = "Legs of Intelligence",
                ItemLevel = 4,
                ItemSlot = Slot.LEGS.ToString(),
                ArmorType = ArmorType.MAIL.ToString(),
                Attributes = new Attributes() { Strength = 5, Dexterity = 10,Intelligence = 15 }
            };


            //Equip sword
            ranger.EquipItem(sword);
            //Equip leather chest
            ranger.EquipItem(LeatherChest);
            //Level up three times. Level will be 4 after.
            ranger.LevelUp();
            ranger.LevelUp();
            ranger.LevelUp();
            //Equip legs with level 4 requirements.
            ranger.EquipItem(legsMail);
            //Print the information of the character.
            ranger.PrintInfo();
            //Get the damage, with current equipment and level.
            Console.WriteLine($"Character damage = {ranger.GetDamage()}" );



        }
    }
}