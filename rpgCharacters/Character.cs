﻿using rpgCharacters.Equipment;
using rpgCharacters.Heroes;

namespace rpgCharacters
{
    public abstract class Character : Attributes
    {

        public string Name { get; set; }
        public string Class { get; set; }
        public int Level { get; set; }
        public Item Equipment { get; set; }

        /// <summary>
        ///     Prints the Character information to console.
        /// </summary>
        public void PrintInfo()
        {
            Console.WriteLine($"A {Class} level {Level} with name {Name}. \n  Strength = {Strength}\n  Dexterity = {Dexterity}\n  Intelligence = {Intelligence}");
            Console.WriteLine($"Weapon:\n Damage = {Damage}\n Attackspeed = {AttackSpeed}");
        }

        /// <summary>
        ///     Get the damage of the character.
        /// </summary>
        /// <returns>The current damage of character.</returns>
        public double GetDamage()
        {
            double dps = Damage * AttackSpeed;
            double rounded_dps = Math.Round(dps, 2);
            double totalDamage;
           
            switch (Class)
            {
                case "Warrior":
                    if(dps == 0)
                    {
                        totalDamage = 1 * (1+ (Strength / 100.0) );
                        return totalDamage;
                    }
                    totalDamage = rounded_dps * (1 +(Strength / 100.0) );
                    return totalDamage;
                case "Rogue":
                    if(dps == 0)
                    {
                        totalDamage = 1 * ( 1 +(Dexterity / 100.0));
                        return totalDamage;
                    }
                    totalDamage = rounded_dps * ( 1+(Dexterity / 100.0) );
                    return totalDamage;
                case "Ranger":
                    if(dps == 0)
                    {
                        totalDamage = 1 * ((Dexterity / 100.0) + 1);
                        return totalDamage;
                    }
                    totalDamage = rounded_dps * ((Dexterity / 100.0) + 1);
                    return totalDamage;
                case "Mage":
                    if(dps == 0)
                    {
                        totalDamage = rounded_dps * ((Intelligence / 100.0) + 1);
                        return totalDamage;
                    }
                    totalDamage = rounded_dps * ((Intelligence / 100.0) + 1);
                    return totalDamage;
                  
                default:
                    return 0;
            }
           
        }

        }
    }

