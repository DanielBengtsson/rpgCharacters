﻿using rpgCharacters.Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpgCharacters.Equipment
{
    //Item class, Inherits from Attributes
    public abstract class Item : Attributes
    {

        //Getters & Setters
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public string ItemSlot { get; set; }
        public string WeaponType { get; set; }
        public string ArmorType { get; set; }


        public Attributes Attributes { get;  set; }
        public WeaponAttributes WeaponAttributes { get; set; }

    }
}
