﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpgCharacters.Equipment
{
    //Class for the Attributes on a Weapon.
    public class WeaponAttributes
    {

        public double Damage { get; set; }
        public double AttackSpeed { get; set; }

        public WeaponAttributes() { }


    }
}
