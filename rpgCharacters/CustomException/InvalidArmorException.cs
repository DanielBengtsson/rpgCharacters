﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpgCharacters.CustomException
{
    public class InvalidArmorException : Exception
    {

        /// <summary>
        ///Should be thrown whenever hero tries to equip wrong armor type or if the level of the armor is to high
        /// </summary>
        public InvalidArmorException() { }

        /// <summary>
        /// Should be thrown whenever hero tries to equip wrong armor type or if the level of the armor is to high
        /// </summary>
        /// <param name="message">Error message.</param>
        public InvalidArmorException(string message) : base(message) { }
    }
}
