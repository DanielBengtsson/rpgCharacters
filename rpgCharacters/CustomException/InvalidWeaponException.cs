﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpgCharacters.CustomException
{
    public class InvalidWeaponException : Exception
    {

        /// <summary>
        /// If a character tries to equip a high level weapon, InvalidWeaponException should be thrown
        /// </summary>
        public InvalidWeaponException() { }

        /// <summary>
        /// If a character tries to equip a high level weapon, InvalidWeaponException should be thrown
        /// </summary>
        /// <param name="message">Error message</param>
        public InvalidWeaponException(string message) : base(message)
        {

        }
    }
}
